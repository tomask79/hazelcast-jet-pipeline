# Hazelcast JET Pipeline API #

It's quite a long time since I tested [Hazelcast jet](https://hazelcast.com/products/jet/) for the [first time](https://bitbucket.org/tomask79/spring-hazelcast-jet-streaming/src/master/). Until the 0.5 version building of the processing steps has been based on the [DAG](https://en.wikipedia.org/wiki/Directed_acyclic_graph) graph computation model, which is quite complex API with kind of long learning curve.    
Anyway since the JET [version 0.5](https://hazelcast.zendesk.com/hc/en-us/articles/115003058931-Release-Notifications-November-28-2017) you can build your jobs using new high level [PipeLine API](https://jaxenter.com/hazelcast-jet-interview-schreiner-139760.html). Which is aimed more at the common Java programmers used to use [java.util.Collection API](https://docs.oracle.com/javase/8/docs/api/java/util/Collection.html) and [Java 8 streaming](https://www.tutorialspoint.com/java8/java8_streams.htm). Excellent move! Let's test some of it. 

# Goals of the demo #

* Howto build an Pipeline job
* BatchStage vs StreamStage 
* Stream enriching techniques
* Howto build your custom sink

# Howto build an Pipeline job #

Just use the static method [create](https://docs.hazelcast.org/docs/jet/0.6/javadoc/com/hazelcast/jet/pipeline/Pipeline.html#create--) of the Pipeline class and build the pipeline. 

```java
		Pipeline p = Pipeline.create();
        p.drawFrom(Sources.<String>list("text"))
			        .flatMap(word -> traverseArray(word.toLowerCase().split("\\W+")))
         			.filter(word -> !word.isEmpty())
					.drainTo(Sinks.logger());
		
		// Start Jet instance
        JetInstance jet = Jet.newJetInstance();
        try {
			// Put some data into text list. 
            List<String> text = jet.getList("text");
			text.add(new String("some words to filter! "));
			// And perform the computation
            jet.newJob(p).join();
        } finally {
            Jet.shutdownAll();
        }
```

This code skelet reads a batch from the finite IList("text") stream, performs the word split, filters out empty strings and drain the output into logger [Sink](https://docs.hazelcast.org/docs/jet/0.6/javadoc/com/hazelcast/jet/pipeline/Sink.html).

To run this sample make class **JetpipelineBasicSampleApplication** as main SpringBootApplication then:

* tomask79@utu:~/bitbprojects/hazelcast-jet-pipeline$ **mvn clean install**
* tomask79@utu:~/bitbprojects/hazelcast-jet-pipeline$ **java -jar target/jetpipeline-0.0.1-SNAPSHOT.jar**

After their nice ascii-art, it's important to see that your node is running the Hazelcast JET instance

	o   o   o   o---o o---o o     o---o   o   o---o o-o-o        o o---o o-o-o
	|   |  / \     /  |     |     |      / \  |       |          | |       |  
	o---o o---o   o   o-o   |     o     o---o o---o   |          | o-o     |  
	|   | |   |  /    |     |     |     |   |     |   |      \   | |       |  
	o   o o   o o---o o---o o---o o---o o   o o---o   o       o--o o---o   o   
    2018-11-12 23:00:09.392  INFO 15681 --- [           main] com.hazelcast.jet.impl.JetService        : [192.168.1.87]:5701 [jet] [0.6] Copyright (c) 2008-2018, Hazelcast, Inc. All Rights Reserved.
    2018-11-12 23:00:09.396  INFO 15681 --- [           main] c.h.s.i.o.impl.OperationExecutorImpl     : [192.168.1.87]:5701 [jet] [0.6] Starting 4 partition threads and 3 generic threads (1 dedicated for priority tasks)
    2018-11-12 23:00:09.400  INFO 15681 --- [           main] c.h.internal.diagnostics.Diagnostics     : [192.168.1.87]:5701 [jet] [0.6] Diagnostics disabled. To enable add -Dhazelcast.diagnostics.enabled=true to the JVM arguments.
    2018-11-12 23:00:09.405  INFO 15681 --- [           main] com.hazelcast.core.LifecycleService      : [192.168.1.87]:5701 [jet] [0.6] [192.168.1.87]:5701 is STARTING
    2018-11-12 23:00:12.179  INFO 15681 --- [           main] com.hazelcast.system                     : [192.168.1.87]:5701 [jet] [0.6] Cluster version set to 3.10
    2018-11-12 23:00:12.180  INFO 15681 --- [           main] c.h.internal.cluster.ClusterService      : [192.168.1.87]:5701 [jet] [0.6] 

    Members {size:1, ver:1} [
	    Member [192.168.1.87]:5701 - ab614ceb-410d-49e2-8257-eb53f537513f this
    ]

and after that your [Pipeline](https://docs.hazelcast.org/docs/jet/0.6/javadoc/com/hazelcast/jet/pipeline/Pipeline.html) job has been submitted as DAG to JET processing engine:

    Start executing job 5725-6d0c-e66f-2e14, execution 35e7-387a-8b65-c9d2, status STARTING dag
    .vertex("listSource(text)").localParallelism(1)
    .vertex("flat-map").localParallelism(4)
    .vertex("filter").localParallelism(4)
    .vertex("loggerSink").localParallelism(1)
    .edge(between("listSource(text)", "flat-map"))
    .edge(between("flat-map", "filter"))
    .edge(between("filter", "loggerSink"))

Expected output of the job:

    Start execution of job 5725-6d0c-e66f-2e14, execution 35e7-387a-8b65-c9d2 from coordinator [192.168.1.87]:5701
    2018-11-12 23:00:12.566  INFO 15681 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#0                 : [192.168.1.87]:5701 [jet] [0.6] some
    2018-11-12 23:00:12.566  INFO 15681 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#0                 : [192.168.1.87]:5701 [jet] [0.6] words
    2018-11-12 23:00:12.566  INFO 15681 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#0                 : [192.168.1.87]:5701 [jet] [0.6] filter
    2018-11-12 23:00:12.567  INFO 15681 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#0                 : [192.168.1.87]:5701 [jet] [0.6] to

# BatchStage vs StreamStage #

These are the fundamental terms of the Pipeline API. Take [Stage](https://docs.hazelcast.org/docs/jet/0.6/javadoc/com/hazelcast/jet/pipeline/Stage.html) as input data proxy of your pipeline job. It takes the input from the source comming into it and emits it downstream of the pipeline. Stage can do that in the finite data amount batches, here we're talking about [BatchStage](https://docs.hazelcast.org/docs/jet/0.6/javadoc/com/hazelcast/jet/pipeline/BatchStage.html) or in the infinite amount of data batches, this is called [StreamStage](https://docs.hazelcast.org/docs/jet/0.6/javadoc/com/hazelcast/jet/pipeline/StreamStage.html).

## BatchStage, two Hazelcast JET nodes example ##

Sample of the BatchStage we saw in the previous sample:

        p.drawFrom(Sources.<String>list("text"))
			        .flatMap(word -> traverseArray(word.toLowerCase().split("\\W+")))

This BatchStage takes finite input from the IList called "text" and proxies it downstream into flatmap function. Maybe this is not that much usable example in the real life, so let's make another one, 
two running HazelcastJET instances. It's going to be application **JetpipelineBatchApplication**, attached in repo, make it as your main SpringBootApp.

```java 

    JetConfig cfg = new JetConfig();
    JetInstance jet = Jet.newJetInstance(cfg);
    if (args.length > 0 && args[0].equals("master")) {            
        System.out.println("Starting jet node as master!");
        IList list = jet.getList("master");
        for (;;) {
            final Date date = new Date();
            printToOutput(date); // printing --> Pushing into list: date 
            list.add(date);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    } else {
        Pipeline p = Pipeline.create();
        BatchStage<Date> listStage = p.drawFrom(Sources.list("master"));
        listStage.drainTo(Sinks.logger());
        jet.newJob(p);
    }
```

* First JET instance is going to be pumping data into IList...java args[0] will contain "master" argument.
* Second JET instance is going to submit Pipeline JOB into the cluster processing the data pumped by the first instance. We will start again same JET code, but without any argument.

This approach has the benefit that both Hazelcast JET instances will have the pumping and the job code on the classpath. But this is NOT always the case in the real life,
see how you can [send the job code](https://docs.hazelcast.org/docs/jet/0.6/manual/#remember-that-a-jet-job-is-distributed) on the wire to the worker jet nodes.

**First instance terminal:**

Let's start HazelcastJET instance and pumping into IList("master")

tomask79@utu:~/bitbprojects/hazelcast-jet-pipeline$ **java -jar target/jetpipeline-0.0.1-SNAPSHOT.jar master**

    .
    .
    Pushing into list: Fri Nov 30 22:05:21 CET 2018
    Pushing into list: Fri Nov 30 22:05:22 CET 2018
    Pushing into list: Fri Nov 30 22:05:23 CET 2018
    Pushing into list: Fri Nov 30 22:05:24 CET 2018
    Pushing into list: Fri Nov 30 22:05:25 CET 2018
    Pushing into list: Fri Nov 30 22:05:26 CET 2018
    Pushing into list: Fri Nov 30 22:05:27 CET 2018
    Pushing into list: Fri Nov 30 22:05:28 CET 2018

**Second instance terminal: **

tomask79@utu:~/bitbprojects/hazelcast-jet-pipeline$ **java -jar target/jetpipeline-0.0.1-SNAPSHOT.jar**

Second instance has joined the cluster.

    .
    .
    Members {size:2, ver:2} [
	    Member [10.130.16.114]:5701 - 39716051-98a1-4e7c-a80e-f2278d69d5de
	    Member [10.130.16.114]:5702 - e53bdf85-9e46-4221-8fab-0ce889e4ba25 this
    ]
    
And submitted the job into cluster. 
    
    2018-11-30 22:05:35.819  INFO 8727 --- [           main] com.hazelcast.core.LifecycleService      : [10.130.16.114]:5702 [jet] [0.6] [10.130.16.114]:5702 is STARTED
    2018-11-30 22:05:38.156  INFO 8727 --- [ration.thread-0] c.h.jet.impl.JobExecutionService         : [10.130.16.114]:5702 [jet] [0.6] Execution plan for job 42d4-8bde-0f22-bd44, execution 89ec-7159-1e79-d14f initialized
    2018-11-30 22:05:38.201  INFO 8727 --- [ration.thread-1] c.h.j.i.o.StartExecutionOperation        : [10.130.16.114]:5702 [jet] [0.6] Start execution of job 42d4-8bde-0f22-bd44, execution 89ec-7159-1e79-d14f from coordinator [10.130.16.114]:5701

Now bear in mind the following things:

* [IList](https://docs.hazelcast.org/docs/3.8/javadoc/com/hazelcast/core/IList.html) isn't partitioned Hazelcast data-structure. It doesn't scale. Only one JET member will have all the content.
* When you submit job into cluster, JET replicates it to the whole cluster.
* Since IList isn't distributed, only one JET instance will be pulling the data...In our case, it's going to be first instance launched with "master" argument.

So switch your attention **back to first instance terminal** and notice:

Second instance has joined the cluster.

    Members {size:2, ver:2} [
	    Member [10.130.16.114]:5701 - 39716051-98a1-4e7c-a80e-f2278d69d5de this
	    Member [10.130.16.114]:5702 - e53bdf85-9e46-4221-8fab-0ce889e4ba25
    ]
    
First instance JET is about to perform the job.
    
    2018-11-30 22:05:35.048  INFO 8673 --- [1_jet.migration] c.h.i.partition.impl.MigrationManager    : [10.130.16.114]:5701 [jet] [0.6] Re-partitioning cluster data... Migration queue size: 271
    Pushing into list: Fri Nov 30 22:05:35 CET 2018
    2018-11-30 22:05:35.956  INFO 8673 --- [ration.thread-1] c.h.jet.impl.JobCoordinationService      : [10.130.16.114]:5701 [jet] [0.6] Starting job 42d4-8bde-0f22-bd44 based on submit request from client
    
Please remember the last push into the IList: **Pushing into list: Fri Nov 30 22:05:37 CET 2018**
    
    Pushing into list: Fri Nov 30 22:05:36 CET 2018
    2018-11-30 22:05:37.413  INFO 8673 --- [1_jet.migration] c.h.i.partition.impl.MigrationThread     : [10.130.16.114]:5701 [jet] [0.6] All migration tasks have been completed, queues are empty.
    Pushing into list: Fri Nov 30 22:05:37 CET 2018
    
Pipeline API job code has been translated into DAG and launched at the first instance.
        
    2018-11-30 22:05:38.005  INFO 8673 --- [cached.thread-3] com.hazelcast.jet.impl.MasterContext     : [10.130.16.114]:5701 [jet] [0.6] Start executing job 42d4-8bde-0f22-bd44, execution 89ec-7159-1e79-d14f, status STARTING
    dag
        .vertex("listSource(master)").localParallelism(1)
        .vertex("loggerSink").localParallelism(1)
        .edge(between("listSource(master)", "loggerSink"))

    2018-11-30 22:05:38.196  INFO 8673 --- [cached.thread-3] c.h.jet.impl.JobExecutionService         : [10.130.16.114]:5701 [jet] [0.6] Execution plan for job 42d4-8bde-0f22-bd44, execution 89ec-7159-1e79-d14f initialized
    2018-11-30 22:05:38.200  INFO 8673 --- [cached.thread-3] c.h.j.i.o.StartExecutionOperation        : [10.130.16.114]:5701 [jet] [0.6] Start execution of job 42d4-8bde-0f22-bd44, execution 89ec-7159-1e79-d14f from coordinator [10.130.16.114]:5701

Now Sink logger printed out the state of IList **in the moment of the JOB launch (notice the last date) **.

    2018-11-30 22:05:38.221  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:20 CET 2018
    2018-11-30 22:05:38.221  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:21 CET 2018
    2018-11-30 22:05:38.221  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:22 CET 2018
    2018-11-30 22:05:38.221  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:23 CET 2018
    2018-11-30 22:05:38.222  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:24 CET 2018
    2018-11-30 22:05:38.222  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:25 CET 2018
    2018-11-30 22:05:38.222  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:26 CET 2018
    2018-11-30 22:05:38.222  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:27 CET 2018
    2018-11-30 22:05:38.222  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:28 CET 2018
    2018-11-30 22:05:38.222  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:29 CET 2018
    2018-11-30 22:05:38.222  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:30 CET 2018
    2018-11-30 22:05:38.223  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:31 CET 2018
    2018-11-30 22:05:38.223  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:32 CET 2018
    2018-11-30 22:05:38.223  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:33 CET 2018
    2018-11-30 22:05:38.223  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:34 CET 2018
    2018-11-30 22:05:38.223  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:35 CET 2018
    2018-11-30 22:05:38.223  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:36 CET 2018
    2018-11-30 22:05:38.223  INFO 8673 --- [ocking.thread-1] c.h.j.i.p.F.loggerSink#1                 : [10.130.16.114]:5701 [jet] [0.6] Fri Nov 30 22:05:37 CET 2018
    2018-11-30 22:05:38.320  INFO 8673 --- [.async.thread-3] com.hazelcast.jet.impl.MasterContext     : [10.130.16.114]:5701 [jet] [0.6] Execution of job 42d4-8bde-0f22-bd44, execution 89ec-7159-1e79-d14f completed in 2,362 ms

First JET instance continues in date pumping.
    
    Pushing into list: Fri Nov 30 22:05:38 CET 2018

To summarize what happended, since IList is an [BatchSource](https://docs.hazelcast.org/docs/jet/latest-dev/javadoc/com/hazelcast/jet/pipeline/BatchSource.html) then first JOB stage is an BatchStage which took **current FINITE state** of the IList, remember last push before the start of the job was Nov 30 22:05:37 CET 2018,
and sent it to Logger [Sink](https://docs.hazelcast.org/docs/jet/0.6/javadoc/com/hazelcast/jet/pipeline/Sink.html)...First instance then continues with pumping into IList.

## StreamStage, two Hazelcast JET nodes example ##

Previous sample has two issues:

* IList isn't distributed data-structure.
* Jet job wasn't processing the IList continuously. Why? Because IList is an BatchSource, finite source. See the following [documentation](https://docs.hazelcast.org/docs/jet/0.6.1/manual/#choose-your-data-sources-and-sinks) for more info.

To fix this let's switch the IList for IMap and job will be listening on the ADD or UPDATE changes from the [IMap's journal](https://docs.hazelcast.org/docs/3.9/manual/html-single/index.html#event-journal).

```java
    @Override
    public void run(String... args) throws Exception {
        JetConfig cfg = new JetConfig();
        cfg.getHazelcastConfig()
                .getMapEventJournalConfig("master")
                .setEnabled(true)
                .setCapacity(1000)
                .setTimeToLiveSeconds(10);
        JetInstance jet = Jet.newJetInstance(cfg);
        if (args.length > 0 && args[0].equals("master")) {
            IMap map = jet.getMap("master");
            for (;;) {
                final Date date = new Date();
                printToOutput(date); // printing --> Pushing into list: date 
                map.put(Math.random(), date);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } else {
            final Pipeline pipeline = createPipeLine();
            jet.newJob(pipeline);
        }
    }

    private Pipeline createPipeLine() {
        Pipeline p = Pipeline.create();
        StreamStage<Map.Entry<Double, Date>> fromMap = p.drawFrom(
                Sources.mapJournal("master", START_FROM_CURRENT));
        fromMap.drainTo(Sinks.logger());
        return p;
    }
```

By the **START_FROM_CURRENT** we say start listening on changes from the start of the job processing. An alternative is **START_FROM_OLDEST** which will start from the first event still being held by journal. See the [journal](https://docs.hazelcast.org/docs/jet/0.6.1/manual/#connector-imdg-journal) documentation. Okay, let's try what is going to happen. Make **JetpipelineMapPartitionedApplication** as your main SpringBootClass and package the jar then...

First terminal:

tomask79@utu:~/bitbprojects/hazelcast-jet-pipeline$ **java -jar target/jetpipeline-0.0.1-SNAPSHOT.jar master**

    .
    .
    018-12-01 13:20:15.581  INFO 17744 --- [ocking.thread-2] c.h.j.i.p.F.loggerSink#0                 : [192.168.1.87]:5701 [jet] [0.6] 0.6688936907716502=Sat Dec 01 13:20:15 CET 2018
    Pushing into list: Sat Dec 01 13:20:16 CET 2018
    Pushing into list: Sat Dec 01 13:20:17 CET 2018
    2018-12-01 13:20:17.583  INFO 17744 --- [ocking.thread-2] c.h.j.i.p.F.loggerSink#0                 : [192.168.1.87]:5701 [jet] [0.6] 0.16382200573038286=Sat Dec 01 13:20:17 CET 2018
    Pushing into list: Sat Dec 01 13:20:18 CET 2018
    2018-12-01 13:20:18.584  INFO 17744 --- [ocking.thread-2] c.h.j.i.p.F.loggerSink#0                 : [192.168.1.87]:5701 [jet] [0.6] 0.8401597358405372=Sat Dec 01 13:20:18 CET 2018
    Pushing into list: Sat Dec 01 13:20:19 CET 2018
    Pushing into list: Sat Dec 01 13:20:20 CET 2018
    2018-12-01 13:20:20.586  INFO 17744 --- [ocking.thread-2] c.h.j.i.p.F.loggerSink#0                 : [192.168.1.87]:5701 [jet] [0.6] 0.014075415635804944=Sat Dec 01 13:20:20 CET 20

Second terminal:


tomask79@utu:~/bitbprojects/hazelcast-jet-pipeline$ **java -jar target/jetpipeline-0.0.1-SNAPSHOT.jar** 

    .
    .
    018-12-01 13:20:14.581  INFO 17796 --- [ocking.thread-2] c.h.j.i.p.F.loggerSink#1                 : [192.168.1.87]:5702 [jet] [0.6] 0.14430924933478928=Sat Dec 01 13:20:14 CET 2018
    2018-12-01 13:20:16.582  INFO 17796 --- [ocking.thread-2] c.h.j.i.p.F.loggerSink#1                 : [192.168.1.87]:5702 [jet] [0.6] 0.5868657786226479=Sat Dec 01 13:20:16 CET 2018
    2018-12-01 13:20:19.585  INFO 17796 --- [ocking.thread-2] c.h.j.i.p.F.loggerSink#1                 : [192.168.1.87]:5702 [jet] [0.6] 0.8186993662055786=Sat Dec 01 13:20:19 CET 2018
    2018-12-01 13:20:21.586  INFO 17796 --- [ocking.thread-2] c.h.j.i.p.F.loggerSink#1                 : [192.168.1.87]:5702 [jet] [0.6] 0.20146359882016696=Sat Dec 01 13:20:21 CET 2018
    2018-12-01 13:20:23.589  INFO 17796 --- [ocking.thread-2] c.h.j.i.p.F.loggerSink#1                 : [192.168.1.87]:5702 [jet] [0.6] 0.8692597134080969=Sat Dec 01 13:20:23 CET 2018 

Okay not only the two JET instances are **streaming the input continously as we push into IMap**, but since IMap is partitioned, both instances processes **their part of the partition**, see the times of the logger sink.

# Stream enriching techniques and Custom Sink #

When you process some stream you often want to enrich stream item with additional data, according to some key like:

* You process items from twitter, and you want to add user info to it.
* You process maybe stream of payments, and you want to add bank info to it.

There are basically three techniques of stream joins, simply read this excellent [blog post](https://blog.hazelcast.com/ways-to-enrich-stream-with-jet/) about that from Marco Topolnik.
In the short form:

* **Hash-join:** It replicates enriching stream to every member of the JET cluster. This has super-high throughput, but enriching data needs to fit into memory.
* **Look-up and transforms:** When you stream data continuously, stream item goes to node which holds their enriching data, to avoid additional network queries.
* **Look-up from external system:** As the name suggests.

## Hash-join demo ##

Let's test the first option, hash-join and have the following [StreamSource](https://docs.hazelcast.org/docs/jet/0.6/javadoc/com/hazelcast/jet/pipeline/StreamSource.html) of objects:

```java
    public class Source implements Serializable {
        private Integer sourceId;
        private Integer additionDataItemId;
        private AdditionalDataItem additionalDataItem;
        private String data;

        /**
         * @param sourceId
         * @param additionDataItemId
         * @param data
         */
        public Source(final Integer sourceId, final Integer additionDataItemId, final String data) {
            this.sourceId = sourceId;
            this.additionDataItemId = additionDataItemId;
            this.data = data;
        }

        public Integer getSourceId() {
            return sourceId;
        }

        public void setSourceId(Integer sourceId) {
            this.sourceId = sourceId;
        }

        public Integer getAdditionDataItemId() {
            return additionDataItemId;
        }

        public void setAdditionDataItemId(Integer additionDataItemId) {
            this.additionDataItemId = additionDataItemId;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }

        public AdditionalDataItem getAdditionalDataItem() {
            return additionalDataItem;
        }

        public Source setAdditionalDataItem(AdditionalDataItem additionalDataItem) {
            this.additionalDataItem = additionalDataItem;
            return this;
        }
    }
```

every Source comming into the [StreamStage](https://docs.hazelcast.org/docs/jet/0.6/javadoc/com/hazelcast/jet/pipeline/StreamStage.html) we want to enrich with object **AdditionalDataItem** via the key in **additionDataItemId**:

```java
    public class AdditionalDataItem implements Serializable {
        private Integer id;
        private String additionalData;

        /**
         * @param id
         * @param additionalData
         */
        public AdditionalDataItem(final Integer id, final String additionalData) {
            this.id = id;
            this.additionalData = additionalData;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getAdditionalData() {
            return additionalData;
        }

        public void setAdditionalData(String additionalData) {
            this.additionalData = additionalData;
        }
    }
```

and again, let's have **one JET instance pumping Source objects into StreamSource** and **second JET instance submiting the Pipeline JOB**, both distinguished by argument from commandline:

```java

    @Override
	public void run(String... args) throws Exception {
		// enriching sample.
		JetConfig cfg = new JetConfig();
		cfg.getHazelcastConfig()
				.getMapEventJournalConfig("master")
				.setEnabled(true)
				.setCapacity(1000)
				.setTimeToLiveSeconds(10);
		JetInstance jet = Jet.newJetInstance(cfg);

		if (args.length > 0 && args[0].equals("master")) {
			final AdditionalDataItem additionalDataItem1 =
					new AdditionalDataItem(1, "Additional_Data1");
			final AdditionalDataItem additionalDataItem2 =
					new AdditionalDataItem(2, "Additional_Data2");
			final AdditionalDataItem additionalDataItem3 =
					new AdditionalDataItem(3, "Additional_Data3");
			final AdditionalDataItem additionalDataItem4 =
					new AdditionalDataItem(4, "Additional_Data4");

			IMap enrich = jet.getMap("enrichData");
			enrich.put(additionalDataItem1.getId(), additionalDataItem1);
			enrich.put(additionalDataItem2.getId(), additionalDataItem2);
			enrich.put(additionalDataItem3.getId(), additionalDataItem3);
			enrich.put(additionalDataItem4.getId(), additionalDataItem4);

			System.out.println("Data enriched.");

			IMap map = jet.getMap("master");
			Random rand = new Random();
			for (;;) {
				Integer addId = rand.nextInt(4)+1;
				Integer sourceId = rand.nextInt();
				final Source source =
						new Source(sourceId, addId, new Date().toString());
				System.out.println("Sourcing source: "+source.getSourceId());
				System.out.println("Done.");
				Thread.sleep(3000);
				map.put(sourceId, source);
			}
		} else {
			final Pipeline pipeline = createPipeLine();
			jet.newJob(pipeline);
		}
	}

	private Pipeline createPipeLine() {
		Pipeline p = Pipeline.create();
		StreamStage<Source> fromMap = p.drawFrom(
				Sources.mapJournal("master",
						Util.mapPutEvents(),
						Util.mapEventNewValue(),
						START_FROM_CURRENT));

		BatchStage<Map.Entry<Integer, AdditionalDataItem>> additionalData =
				p.drawFrom(Sources.map("enrichData"));

		StreamStage<Source> enriched = fromMap.hashJoin
				(
						additionalData,
						JoinClause.joinMapEntries(Source::getAdditionDataItemId),
						Source::setAdditionalDataItem
				);

		enriched.drainTo(CustomSink.newSink());
		return p;
	}
``` 

notice were we drain Source items in the end. Except of the predefined JET [Sinks](https://docs.hazelcast.org/docs/jet/0.6/javadoc/com/hazelcast/jet/pipeline/Sinks.html) of course JET gives you posibility to write your own **custom Sink**.    
Internet is full of only one example, draining into [JDBC target](https://blog.hazelcast.com/hazelcast-jet-tutorial-building-custom-jdbc-sinks/). So I did something slightly different, logging Sink printing Source items in some format:

```java

    public class CustomSink {
        public static Sink<Source> newSink() {
            return Sinks.<String, Source>builder((jetInstance) -> "No need of context object.")
                        .onReceiveFn(CustomSink::printObject)
                        .flushFn(CustomSink::onFlush)
                        .destroyFn(CustomSink::onClose)
                        .build();
        }

        public static void printObject(String ctx, Source source) {
            System.out.println("Sinking id:"+source.getSourceId());
            System.out.println("Sinking data:"+source.getData());
            System.out.println("Additional item:"+source.getAdditionDataItemId());
            System.out.println("Source additional item:"+source.getAdditionalDataItem().getAdditionalData());
            System.out.println("-------------------------");
        }

        public static void onClose(String ctx) {
            System.out.println("Closing sink!");
        }

        public static void onFlush(String ctx) {
            System.out.println("Flushing sink!");
        }
    }
```

JET always requires context object in the Sink callbacks, see [SinkBuilder](https://docs.hazelcast.org/docs/jet/0.6/javadoc/com/hazelcast/jet/pipeline/SinkBuilder.html). Makes sense for the JDBC Sink,    
but it's completely unnecesarry when doing simple non-db sink. 

## Running the instances ##

Make the class **JetpipelineApplicationMapEnriching** as the main SpringBoot app and package the app:

tomask79@utu:~/bitbprojects/hazelcast-jet-pipeline$ **mvn clean install**

now **start the StreamSource and it's hash-join enriching **:

tomask79@utu:~/bitbprojects/hazelcast-jet-pipeline$ **java -jar target/jetpipeline-0.0.1-SNAPSHOT.jar master**

open second terminal for submiting the job:

tomask79@utu:~/bitbprojects/hazelcast-jet-pipeline$ **java -jar target/jetpipeline-0.0.1-SNAPSHOT.jar**

**Second terminal** should show data flowing into our CustomSink for Source object, notice that stream items are enriched by AdditionalData:

    -------------------------
    Flushing sink!
    Sinking id:-1347237571
    Sinking data:Fri Dec 07 23:12:04 CET 2018
    Additional item:3
    Source additional item:Additional_Data3
    -------------------------
    Flushing sink!
    Sinking id:-1419683241
    Sinking data:Fri Dec 07 23:12:07 CET 2018
    Additional item:2
    Source additional item:Additional_Data2
    -------------------------
    Flushing sink!
    Sinking id:2016439713
    Sinking data:Fri Dec 07 23:12:10 CET 2018
    Additional item:1
    Source additional item:Additional_Data1
    -------------------------

**First terminal** also shows data flowing into CustomSink, but also still pumping into [StreamStage](https://docs.hazelcast.org/docs/jet/latest/javadoc/com/hazelcast/jet/pipeline/StreamStage.html):

    -------------------------
    Flushing sink!
    Sourcing source: -640482730
    Done.
    Sourcing source: 2142907597
    Done.
    Sinking id:-640482730
    Sinking data:Fri Dec 07 23:11:43 CET 2018
    Additional item:2
    Source additional item:Additional_Data2
    -------------------------
    Flushing sink!
    Sourcing source: 1236159052
    Done.
    Sourcing source: 1457014658
    Done.
    Sinking id:1236159052
    Sinking data:Fri Dec 07 23:11:49 CET 2018
    Additional item:3
    Source additional item:Additional_Data3
    -------------------------

# Summary #

There are few things you should not miss when evaluating Hazelcast JET for your needs:

* Hazelcast JET supports [fault tollerance](https://blog.hazelcast.com/hazelcast-jet-0-5-fault-tolerant-stateful-stream-processing-made-easy/)
* Hazelcast JET supports [split brain protection](https://docs.hazelcast.org/docs/jet/latest-dev/manual/#split-brain-protection)
* Hazelcast JET supports [Docker and K8s deployments](https://docs.hazelcast.org/docs/jet/latest-dev/manual/#deployment-using-docker).

Okay, it's not all, but enough for you to start. Happy JETing :)

regards

Tomas