package com.example.jetpipeline;

import com.example.jetpipeline.model.AdditionalDataItem;
import com.example.jetpipeline.model.Source;
import com.example.jetpipeline.sinks.CustomSink;
import com.hazelcast.core.IMap;
import com.hazelcast.jet.Jet;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.Util;
import com.hazelcast.jet.config.JetConfig;
import com.hazelcast.jet.pipeline.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;
import java.util.Map;
import java.util.Random;

import static com.hazelcast.jet.pipeline.JournalInitialPosition.START_FROM_CURRENT;


/**
 * Demo1. Pipeline IMap processing
 * Demo2. Pipeline IMap enriching, hash join.
 *
 * @author tomask79
 */
@SpringBootApplication
public class JetpipelineApplicationMapEnriching implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(JetpipelineApplicationMapEnriching.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// enriching sample.
		JetConfig cfg = new JetConfig();
		cfg.getHazelcastConfig()
				.getMapEventJournalConfig("master")
				.setEnabled(true)
				.setCapacity(1000)
				.setTimeToLiveSeconds(10);
		JetInstance jet = Jet.newJetInstance(cfg);

		if (args.length > 0 && args[0].equals("master")) {
			final AdditionalDataItem additionalDataItem1 =
					new AdditionalDataItem(1, "Additional_Data1");
			final AdditionalDataItem additionalDataItem2 =
					new AdditionalDataItem(2, "Additional_Data2");
			final AdditionalDataItem additionalDataItem3 =
					new AdditionalDataItem(3, "Additional_Data3");
			final AdditionalDataItem additionalDataItem4 =
					new AdditionalDataItem(4, "Additional_Data4");

			IMap enrich = jet.getMap("enrichData");
			enrich.put(additionalDataItem1.getId(), additionalDataItem1);
			enrich.put(additionalDataItem2.getId(), additionalDataItem2);
			enrich.put(additionalDataItem3.getId(), additionalDataItem3);
			enrich.put(additionalDataItem4.getId(), additionalDataItem4);

			System.out.println("Data enriched.");

			IMap map = jet.getMap("master");
			Random rand = new Random();
			for (;;) {
				Integer addId = rand.nextInt(4)+1;
				Integer sourceId = rand.nextInt();
				final Source source =
						new Source(sourceId, addId, new Date().toString());
				System.out.println("Sourcing source: "+source.getSourceId());
				System.out.println("Done.");
				Thread.sleep(3000);
				map.put(sourceId, source);
			}
		} else {
			final Pipeline pipeline = createPipeLine();
			jet.newJob(pipeline);
		}
	}

	private Pipeline createPipeLine() {
		Pipeline p = Pipeline.create();
		StreamStage<Source> fromMap = p.drawFrom(
				Sources.mapJournal("master",
						Util.mapPutEvents(),
						Util.mapEventNewValue(),
						START_FROM_CURRENT));

		BatchStage<Map.Entry<Integer, AdditionalDataItem>> additionalData =
				p.drawFrom(Sources.map("enrichData"));

		StreamStage<Source> enriched = fromMap.hashJoin
				(
						additionalData,
						JoinClause.joinMapEntries(Source::getAdditionDataItemId),
						Source::setAdditionalDataItem
				);

		enriched.drainTo(CustomSink.newSink());
		return p;
	}
}
