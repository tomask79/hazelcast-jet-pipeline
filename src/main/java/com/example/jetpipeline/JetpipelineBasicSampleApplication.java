package com.example.jetpipeline;

import com.hazelcast.jet.Jet;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.pipeline.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;
import java.util.List;

import static com.hazelcast.jet.Traversers.traverseArray;

//@SpringBootApplication
public class JetpipelineBasicSampleApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(JetpipelineBasicSampleApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Pipeline p = Pipeline.create();
        p.drawFrom(Sources.<String>list("text"))
                .flatMap(word -> traverseArray(word.toLowerCase().split("\\W+")))
                .filter(word -> !word.isEmpty())
                .drainTo(Sinks.logger());

        // Start Jet instance
        JetInstance jet = Jet.newJetInstance();
        try {
            // Put some data into text list.
            List<String> text = jet.getList("text");
            text.add(new String("some words to filter! "));
            // And perform the computation
            jet.newJob(p).join();
        } finally {
            Jet.shutdownAll();
        }
    }
}