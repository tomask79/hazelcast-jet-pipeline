package com.example.jetpipeline;

import com.hazelcast.core.IList;
import com.hazelcast.jet.Jet;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.config.JetConfig;
import com.hazelcast.jet.pipeline.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

//@SpringBootApplication
public class JetpipelineBatchApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(JetpipelineBatchApplication.class, args);
    }

    /**
     * IList pipeline processing.
     */
    @Override
    public void run(String... args) throws Exception {
        JetConfig cfg = new JetConfig();
        JetInstance jet = Jet.newJetInstance(cfg);
        if (args.length > 0 && args[0].equals("master")) {
            System.out.println("Starting jet node as master!");
            IList list = jet.getList("master");
            for (; ; ) {
                final Date date = new Date();
                System.out.println("Pushing into list: " + date);
                list.add(date);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } else {
            final Pipeline pipeline = createPipeLine();
            jet.newJob(pipeline);
        }
    }

    private Pipeline createPipeLine() {
        Pipeline p = Pipeline.create();
        BatchStage<Date> listStage = p.drawFrom(Sources.list("master"));
        listStage.drainTo(Sinks.logger());
        return p;
    }
}