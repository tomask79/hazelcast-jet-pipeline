package com.example.jetpipeline;

import com.hazelcast.core.IMap;
import com.hazelcast.jet.pipeline.Pipeline;
import com.hazelcast.jet.pipeline.Sinks;
import com.hazelcast.jet.pipeline.Sources;
import org.springframework.boot.SpringApplication;
import com.hazelcast.jet.Jet;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.config.JetConfig;
import com.hazelcast.jet.pipeline.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;
import java.util.Map;

import static com.hazelcast.jet.pipeline.JournalInitialPosition.START_FROM_CURRENT;
import static com.hazelcast.jet.pipeline.JournalInitialPosition.START_FROM_OLDEST;

//@SpringBootApplication
public class JetpipelineMapPartitionedApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(JetpipelineMapPartitionedApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        JetConfig cfg = new JetConfig();
        cfg.getHazelcastConfig()
                .getMapEventJournalConfig("master")
                .setEnabled(true)
                .setCapacity(1000)
                .setTimeToLiveSeconds(10);
        JetInstance jet = Jet.newJetInstance(cfg);
        if (args.length > 0 && args[0].equals("master")) {
            System.out.println("Starting jet node as master!");
            IMap map = jet.getMap("master");
            for (;;) {
                final Date date = new Date();
                System.out.println("Pushing into list: "+date);
                map.put(Math.random(), date);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } else {
            final Pipeline pipeline = createPipeLine();
            jet.newJob(pipeline);
        }
    }

    private Pipeline createPipeLine() {
        Pipeline p = Pipeline.create();
        StreamStage<Map.Entry<Double, Date>> fromMap = p.drawFrom(
                Sources.mapJournal("master", START_FROM_OLDEST));
        fromMap.drainTo(Sinks.logger());
        return p;
    }
}
