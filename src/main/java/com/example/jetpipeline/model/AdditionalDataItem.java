package com.example.jetpipeline.model;

import java.io.Serializable;

/**
 * @author tomask79
 */
public class AdditionalDataItem implements Serializable {
    private Integer id;
    private String additionalData;

    /**
     * @param id
     * @param additionalData
     */
    public AdditionalDataItem(final Integer id, final String additionalData) {
        this.id = id;
        this.additionalData = additionalData;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }
}
