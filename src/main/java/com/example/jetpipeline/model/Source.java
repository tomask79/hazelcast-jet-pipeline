package com.example.jetpipeline.model;

import java.io.Serializable;

/**
 * Main source of events.
 *
 * @author tomask79
 */
public class Source implements Serializable {
    private Integer sourceId;
    private Integer additionDataItemId;
    private AdditionalDataItem additionalDataItem;
    private String data;

    /**
     * @param sourceId
     * @param additionDataItemId
     * @param data
     */
    public Source(final Integer sourceId, final Integer additionDataItemId, final String data) {
        this.sourceId = sourceId;
        this.additionDataItemId = additionDataItemId;
        this.data = data;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public Integer getAdditionDataItemId() {
        return additionDataItemId;
    }

    public void setAdditionDataItemId(Integer additionDataItemId) {
        this.additionDataItemId = additionDataItemId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public AdditionalDataItem getAdditionalDataItem() {
        return additionalDataItem;
    }

    public Source setAdditionalDataItem(AdditionalDataItem additionalDataItem) {
        this.additionalDataItem = additionalDataItem;
        return this;
    }
}
