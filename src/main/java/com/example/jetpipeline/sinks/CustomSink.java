package com.example.jetpipeline.sinks;

import com.example.jetpipeline.model.Source;
import com.hazelcast.jet.pipeline.Sink;
import com.hazelcast.jet.pipeline.Sinks;

public class CustomSink {
    public static Sink<Source> newSink() {
        return Sinks.<String, Source>builder((jetInstance) -> "No need of context object.")
                .onReceiveFn(CustomSink::printObject)
                .flushFn(CustomSink::onFlush)
                .destroyFn(CustomSink::onClose)
                .build();
    }

    public static void printObject(String ctx, Source source) {
        System.out.println("Sinking id:"+source.getSourceId());
        System.out.println("Sinking data:"+source.getData());
        System.out.println("Additional item:"+source.getAdditionDataItemId());
        System.out.println("Source additional item:"+source.getAdditionalDataItem().getAdditionalData());
        System.out.println("-------------------------");
    }

    public static void onClose(String ctx) {
        System.out.println("Closing sink!");
    }

    public static void onFlush(String ctx) {
        System.out.println("Flushing sink!");
    }
}
